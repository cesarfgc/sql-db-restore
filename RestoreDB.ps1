$iamrole = "EC2S3readTest"
$bucketname = "cfgc-sqlbackup/testvm"
$bakfile = "Test.bak"
$scriptfile = "Insert.sql"
$dest = "C:\users\administrator\Documents\"
$SqlServerName = "localhost"
$logpath = "C:\temp\"
$datapath = "C:\temp\"
$username = "testuser"
$password = "VRM8i6H8RCByezk5YDLm"
$outputfile = "C:\users\administrator\Documents\output.json"

if ((Get-Module -ListAvailable -Name AWSPowerShell) -and (get-command *S3*)){
    write-host "AWSPowerShell module installed" -ForegroundColor green
}else{
    write-host "AWSPowerShell module NOT installed, proceeding to install..." -ForegroundColor Yellow
    Install-Module  -Name AWSPowerShell -Force -Confirm:$false
}

if ((test-path "C:\Program Files\Amazon\AWSCLI\aws.exe") -or (test-path "C:\Program Files (x86)\Amazon\AWSCLI\aws.exe")){
    write-host "AWSCli installed" -ForegroundColor green
}else{
    write-host "AWSCli NOT installed, proceeding to install..." -ForegroundColor Yellow
    Invoke-WebRequest -Uri https://s3.amazonaws.com/aws-cli/AWSCLI64.msi -Outfile $dest\AWSCLI64.msi
    start-process "msiexec" -argumentlist "/i C:\AWSCLI64.msi /quiet /norestart" -wait
}

$s3creds = invoke-restmethod http://169.254.169.254/latest/meta-data/iam/security-credentials/$iamrole
$error.clear()
read-S3Object -BucketName $bucketname -key $bakfile -file ($dest+$bakfile) -ErrorAction SilentlyContinue -AccessKey $s3creds.AccessKeyId -SecretKey $s3creds.SecretAccessKey -SessionToken $s3creds.Token
if($error.Count -gt 0){Write-host "Failed to download $bakfile file exception: $($error[0].Exception)" ;$error.clear()}else{Write-host "$bakfile file downloaded successfully"}

read-S3Object -BucketName $bucketname -key $scriptfile -file ($dest+$scriptfile) -ErrorAction SilentlyContinue -AccessKey $s3creds.AccessKeyId -SecretKey $s3creds.SecretAccessKey -SessionToken $s3creds.Token
if($error.Count -gt 0){Write-host "Failed to download $scriptfile file exception: $($error[0].Exception)";$error.clear()}else{Write-host "$scriptfile file downloaded successfully"}


$results = SQLCMD -E -S $SqlServerName -Q "restore headeronly from disk = '$($dest+$bakfile)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.databasename -ne $null}		

$DBName = $results.databasename

$results = SQLCMD -E -S $SqlServerName -Q "restore filelistonly from disk = '$($dest+$bakfile)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.type -ne $null}		
           
foreach($result in $results) {

    switch($result.type){
    D {$dataname = $result.LogicalName;$dataphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
    L {$Logname = $result.LogicalName;$logphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
    
    }
}   

$RestoreQuery = "RESTORE DATABASE [$DBName] FROM DISK = '$($dest+$bakfile)' WITH MOVE "
$RestoreQuery +="'$($dataname)' TO '$($datapath+$dataphname)', MOVE '$($Logname)' TO '$($datapath+$logphname)', RECOVERY, REPLACE, STATS = 10;"
Write-host "Running restore query: $($RestoreQuery)"
$restore = SQLCMD -E -S $SqlServerName -Q $RestoreQuery -W                
if(($restore|select -last 1|where{$_ -match "successfully"}).count -eq 0 ){
    write-host "Restore failed" -ForegroundColor red
    $restore
}else{
    write-host "Restore succedded" -ForegroundColor green
    $restore|select -last 1
}
Write-host "Running script..."
$scriptrun = SQLCMD -E -S $SqlServerName -i $($dest+$scriptfile) -W -b                
if($lastexitcode -eq 0 ){
    write-host "Script succeeded" -ForegroundColor green
}else{
    write-host "Script failed" -ForegroundColor red
}
$scriptrun   
  
sqlcmd -S $SqlServerName -Q "use [$DBName]; CREATE LOGIN $username WITH PASSWORD = '$($password)';CREATE USER $username FOR LOGIN $username;EXEC sp_addrolemember N'db_owner', N'$($username)';"

if($lastexitcode -eq 0 ){
    write-host "Create user succeeded" -ForegroundColor green
}else{
    write-host "Create user failed" -ForegroundColor red
}
$instance = (Invoke-WebRequest http://169.254.169.254/latest/dynamic/instance-identity/document -useBasicParsing -ErrorAction SilentlyContinue).content|convertfrom-json
$string=@"
{
  "connectionString": 
"Provider=SQLNCLI11;Server=$($instance.privateip);Database=$DBname;Uid=$username;Pwd=$password;"
}
"@
set-content $outputfile -value $string -force